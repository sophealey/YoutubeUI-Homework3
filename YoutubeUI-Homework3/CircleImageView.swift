//
//  CircleImageView.swift
//  YoutubeUI-Homework3
//
//  Created by Sophealey on 12/10/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit
@IBDesignable
class CircleImageView: UIButton {

    override func draw(_ rect: CGRect) {
        let path = UIBezierPath(ovalIn: rect)
        UIColor.blue.setFill()
        path.fill()
    }
}
