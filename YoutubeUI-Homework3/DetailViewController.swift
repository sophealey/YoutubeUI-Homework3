//
//  DetailViewController.swift
//  YoutubeUI-Homework3
//
//  Created by Sophealey on 12/8/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var videoImageView: UIImageView!
    
    @IBOutlet weak var videoDetailTitleTableView: UITableView!
    
    var videoScreen:String?
    var videoTitle:String?
    var profile:String?
    var channel:String?
    var viewNum:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        videoDetailTitleTableView.delegate=self
        videoDetailTitleTableView.dataSource = self
        videoImageView.image = UIImage(named:videoScreen!)
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
   
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = Bundle.main.loadNibNamed("VideoDetailTitleCell", owner: self, options:nil)?.first as! VideoDetailTitleCell
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            cell.configureCell(
                videoDetailTitle: self.videoTitle!,
                viewNum: self.viewNum!
                )
            return cell
        }else if indexPath.row == 1{
            let cell = Bundle.main.loadNibNamed("ActionCell", owner: self, options:nil)?.first as! ActionCell
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            return cell
        }else if indexPath.row == 2{
            let cell = Bundle.main.loadNibNamed("SubscribeCell", owner: self, options:nil)?.first as! SubscribeCell
            
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            cell.configure(profile: profile!, channel: channel!)
            return cell
        }
        return UITableViewCell()
        
      
        
    }

}
