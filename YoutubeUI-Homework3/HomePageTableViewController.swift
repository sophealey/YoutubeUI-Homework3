//
//  HomePageTableViewController.swift
//  YoutubeUI-Homework3
//
//  Created by Sophealey on 12/7/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit

class HomePageTableViewController: UITableViewController {

    @IBOutlet var youtubeTableView: UITableView!
    var videoList:[Video] = [
        Video.init(videoScreen: "iPhoneX",
                   profile: "mkbhd",
                   videoTitle: "iPhone X vs Note 8 vs Pixel 2 vs the Hasselblad X1D. It's closer than you might think!",
                   channel: "Marques Brownlee",
                   view: "3M Views",
                   duration: "2 day ago"),
        Video.init(videoScreen: "face-detector",
                   profile: "the-swift-guy",
                   videoTitle: "Face Detection - Making A Face Detector In Xcode 9 (Swift 4)",
                   channel: "The Swift Guy",
                   view: "5M Views",
                   duration: "7 day ago"),
        Video.init(videoScreen: "safe-area",
                   profile: "lba",
                   videoTitle: "Swift: Watch Out for Safe Areas, UIStackViews Rock!,UIViewController's safe areas.",
                   channel: "Lets Build That App",
                   view: "10M Views",
                   duration: "10 day ago"),
        Video.init(videoScreen: "tv",
                   profile: "mkbhd",
                   videoTitle: "LG 4K OLED W: Possibly the most glorious wall-mounted TV of all time.",
                   channel: "Marques Brownlee",
                   view: "10M Views",
                   duration: "6 day ago")]
    
    //work with Barbutton
    let youtubeBarButton:UIBarButtonItem = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "youtube-logo"), for: .normal)
        let newBarButton = UIBarButtonItem(customView: button)
        let width = newBarButton.customView?.widthAnchor.constraint(equalToConstant: 100)
        width?.isActive = true
        let height = newBarButton.customView?.heightAnchor.constraint(equalToConstant: 20)
        height?.isActive = true
   
        return newBarButton
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.cellLayoutMarginsFollowReadableWidth = false
        self.navigationItem.leftBarButtonItem = youtubeBarButton
        
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videoList.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
           
            let cell = Bundle.main.loadNibNamed("VideoCell", owner: self, options:nil)?.first as? VideoCell
            cell?.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            
        cell?.configureCell(
            videoScreen:videoList[indexPath.row].videoScreen!,
            profile:videoList[indexPath.row].profile,
            videoTitle:videoList[indexPath.row].videoTitle!,
            channel: videoList[indexPath.row].channel!,
            view: videoList[indexPath.row].view!,
            duration:videoList[indexPath.row].duration!
        )
            return cell!
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let videoDetail = self.storyboard?.instantiateViewController(withIdentifier: "videoPlayerStoryBoard") as! DetailViewController
        videoDetail.videoScreen = videoList[indexPath.row].videoScreen
        videoDetail.videoTitle = videoList[indexPath.row].videoTitle
        videoDetail.profile = videoList[indexPath.row].profile
        videoDetail.channel = videoList[indexPath.row].channel
        videoDetail.viewNum = videoList[indexPath.row].view
        
       self.navigationController?.pushViewController(videoDetail, animated: true)
    }
}
