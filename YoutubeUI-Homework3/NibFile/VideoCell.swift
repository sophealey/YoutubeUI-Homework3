//
//  VideoCell.swift
//  YoutubeUI-Homework3
//
//  Created by Sophealey on 12/7/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit

class VideoCell: UITableViewCell {

    @IBOutlet weak var videoScreenImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var videoTitleLabel: UILabel!
    @IBOutlet weak var channelLabel: UILabel!
    @IBOutlet weak var viewLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func configureCell(videoScreen:String,profile:String,videoTitle:String,channel:String,view:String,duration:String){
        
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2
        self.profileImageView.clipsToBounds = true
        
        //self.videoScreenImageView.image = videoScreen
        self.videoScreenImageView.image = UIImage(named: videoScreen)
        self.profileImageView.image = UIImage(named: profile)
        self.videoTitleLabel.text = videoTitle
        self.channelLabel.text = channel
        self.viewLabel.text = view
        self.durationLabel.text = duration
        
    }
    
}
