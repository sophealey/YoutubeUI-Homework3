//
//  VideoDetailTitleCell.swift
//  YoutubeUI-Homework3
//
//  Created by Sophealey on 12/8/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit

class VideoDetailTitleCell: UITableViewCell {

    @IBOutlet weak var videoDetailTitleLabel: UILabel!
    
    @IBOutlet weak var viewNumLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(videoDetailTitle:String,viewNum:String){
        videoDetailTitleLabel.text = videoDetailTitle
        viewNumLabel.text = viewNum
    }
    
}
