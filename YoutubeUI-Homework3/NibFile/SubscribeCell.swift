//
//  SubscribeCell.swift
//  YoutubeUI-Homework3
//
//  Created by Sophealey on 12/10/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit

class SubscribeCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var channelLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func configure(profile:String,channel:String){
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2
        self.profileImageView.clipsToBounds = true
        
        self.profileImageView.image = UIImage(named:profile)
        self.channelLabel.text = channel
    }
    
}
