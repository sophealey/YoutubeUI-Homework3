//
//  Video.swift
//  YoutubeUI-Homework3
//
//  Created by Sophealey on 12/7/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import Foundation
class Video{
    var videoScreen:String?
    var profile:String
    var videoTitle:String?
    var channel:String?
    var view:String?
    var duration:String?
    
    init(videoScreen:String,profile:String,videoTitle:String,channel:String,view:String,duration:String) {
        self.videoScreen = videoScreen
        self.profile = profile
        self.videoTitle = videoTitle
        self.channel = channel
        self.view = view
        self.duration = duration
    }
    
}
